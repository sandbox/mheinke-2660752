<?php
/**
 * @file
 * hubspot_form_entity.features.inc
 */

/**
 * Implements hook_node_info().
 */
function hubspot_form_entity_node_info() {
  $items = array(
    'hubspot_form' => array(
      'name' => t('Hubspot Form'),
      'base' => 'node_content',
      'description' => t('Creates a node that holds a hubspot form.'),
      'has_title' => '1',
      'title_label' => t('Form Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
